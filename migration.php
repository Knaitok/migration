<?php
// Объявляем нужные константы
const DB_HOST = 'localhost';
const DB_USER = 'root';
const DB_PASSWORD = 'root';
const DB_NAME = 'durandin';
const DB_TABLE_VERSION = 'versions';

// Подключаемся к базе данных
function connectDB() {
        $errorMessage = 'Невозможно подключиться к серверу базы данных';
        $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD);
        if (!$conn){
            throw new Exception($errorMessage);
        }else{
            return $conn;
        }
    }
// Получаем список файлов для миграций
function getMigrationFiles($conn) {
    $sqlFolder = str_replace('\\', '/', dirname(__FILE__).'/');
    //файлы с абсолютным путем
    $allFiles = glob($sqlFolder . '*.sql');
    //название файла
    $nameFile = array();
    foreach($allFiles as $file){
        array_push($nameFile,basename($file)); //basename -- название файла без абсолютного пути
    }
    //Проверка на существование бд
    $db_list = $conn->query("SHOW DATABASES;");
    $isExistDB = false;
    foreach($db_list as $itemList) 
    {
        foreach($itemList as $item) {
            if($item == DB_NAME) {
                $isExistDB = true;
            }
        } 
    }
    if(!$isExistDB) {
        FirstMigration($conn,$allFiles);
    } 
    $conn->query('use '.DB_NAME.';');
    // Ищем уже существующие миграции
    // Выбираем из таблицы versions все названия файлов
    $query = sprintf('select name from %s', DB_TABLE_VERSION);
    //Получаем значения из таблицы версий миграции
    $result = $conn->query($query);
    $data=$result->fetch_assoc();
    // Возвращаем файлы, которых еще нет в таблице versions
    return array_diff($nameFile, $data); //array_diff -- разница между массивами
    }

function FirstMigration($conn,$files) {
    //Создать бд и загрузить данные
    $conn->query("CREATE DATABASE ".DB_NAME.";");
    $conn->query('use '.DB_NAME.';');
    migrate($conn,$files);
    }
    //
function migrate($conn, $files) {
    foreach($files as $file){
        $f = fopen($file,"r+");
        $sqlFile = fread($f, filesize($file));
        $sqlArray = explode(';',$sqlFile);
        //выполнение запросов, которые в файле
        foreach($sqlArray as $stmt){
            $result = $conn->query($stmt.';');
        }
            $baseName = basename($file);
            $command = sprintf('INSERT INTO %s (name) values ("%s")',DB_TABLE_VERSION,$baseName);
            $conn->query($command); 
        }
        //загрузка бд данными
       /* $ostatki = 9000;
        for($i=1;$i<=1000; $i++){
            $command = sprintf('INSERT INTO tovar (name) values ("Товар %s");',$i);
            $command .= sprintf('INSERT INTO price (name) values ("0.5");');
            $ostatki = $ostatki-0.5;
            $command .= sprintf('INSERT INTO ostatok (name) values ("%s");',$ostatki);
            $command .= sprintf('INSERT INTO document (id_tovar,id_price,id_ostatok) VALUES ("%s","%s","%s");',$i,$i,$i);
            $conn->multi_query($command);
            while($conn->next_result()) {;}
        }*/
    }
$conn=connectDB();
$files = getMigrationFiles($conn);   
// Проверяем, есть ли новые миграции
    if (empty($files)) {
        echo 'Ваша база данных в актуальном состоянии.';
    } else {
            migrate($conn, $files);
            echo 'База данных обновлена';       
        }   
?>