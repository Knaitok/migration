
CREATE TABLE `document` (
  `id_document` int(11) NOT NULL AUTO_INCREMENT,
  `id_tovar` int(11) DEFAULT NULL,
  `id_price` int(11) DEFAULT NULL,
  `id_ostatok` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_document`),
  KEY `id_tovar` (`id_tovar`),
  KEY `id_price` (`id_price`),
  KEY `id_ostatok` (`id_ostatok`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



CREATE TABLE `ostatok` (
  `id_ostatok` int(11) NOT NULL AUTO_INCREMENT,
  `name` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id_ostatok`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;




CREATE TABLE `price` (
  `id_price` int(11) NOT NULL AUTO_INCREMENT,
  `name` decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (`id_price`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `tovar` (
  `id_tovar` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_tovar`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `versions` (
    `id` INT(10) unsigned NOT NULL auto_increment,
    `name` VARCHAR(255) NOT NULL,
    `created` timestamp default current_timestamp,
    primary key (id)
)ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;