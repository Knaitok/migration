<!doctype html>
<html lang="ru">
<head>
<title>информация о товаре</title>
</head>
<body>
<?php

// Объявляем нужные константы
const DB_HOST = 'localhost';
const DB_USER = 'root';
const DB_PASSWORD = 'root';
const DB_NAME = 'durandin';
const DB_TABLE_VERSION = 'versions';
$conn = connectDB();
echo "<p> Таблица товары </p>";
$result = $conn->query("SELECT id_document, tovar.name as tovar,price.name as price,ostatok.name as ostatok 
                        FROM document,tovar,price,ostatok
                        WHERE tovar.id_tovar = document.id_document AND
                        price.id_price = document.id_price AND
                        ostatok.id_ostatok = document.id_ostatok;");
echo '<table border="1">';
echo '<th>Номер</th>';
echo '<th>Название товара</th>';
echo '<th>Цена</th>';
echo '<th>Остаток</th>';
while ($row = $result->fetch_array()) {
    echo '<tr>';	
    echo '<td>'.$row['id_document']. '</td>';
    echo '<td>'.$row['tovar'].'</td>';
    echo '<td>'.$row['price'].'</td>';
    echo '<td>'.$row['ostatok'].'</td>';
    echo '</tr>';
}
echo '</table>';


// Подключаемся к базе данных
function connectDB() {
        $errorMessage = 'Невозможно подключиться к серверу базы данных';
        $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD,DB_NAME);
        if (!$conn){
            throw new Exception($errorMessage);
        }else{
            return $conn;
        }
    }



?>
</body>
</html>
